# Cargo DynaBindgen

[![pipeline status](https://gitlab.com/Michael-F-Bryan/dyna-bindgen/badges/master/pipeline.svg)](https://gitlab.com/Michael-F-Bryan/dyna-bindgen/pipelines)

[**(Rendered Documentation)**](https://michael-f-bryan.gitlab.io/dyna-bindgen)

A `cargo` subcommand for generating FFI bindings for your crate by leveraging
`rustc` for whole-crate analysis.

The `dyna-bindgen` tool is designed to create bindings which load a shared
library at runtime using [dlopen] or [LoadLibrary] \(depending on the
platform), and to make it as easy as possible to create bindings for other
languages. This is in comparison to other tools like [cbindgen] which focus
on generating declarations for a select few languages (typically C and C++)
for use with static linking at compile time or dynamically linking when the
program gets loaded into memory.

By using `rustc`'s internals for crate analysis binding generators get access 
to everything the compiler knows, as well as the powerful infrastructure already
in place for notifying users of errors or warnings.

## Getting Started

Due to its very nature, you will only be able to install the `cargo
dyna-bindgen` subcommand with a nightly compiler (we *are* using `rustc`
internals after all).

```
$ rustup default nightly
```

Once you switch to nightly, grab a copy of the source code

```
$ git clone https://gitlab.com/Michael-F-Bryan/dyna-bindgen
```

And install it

```
$ cd dyna-bindgen
$ cargo install
```

You should then be able to run the tool on your crate

```
$ cd path/to/my/awesome/crate
$ cargo dyna-bindgen -v --generator rust
Mar 04 03:23:34.183 INFO Starting dyna-bindgen, verbosity: 1, manifest_path: None
Mar 04 03:23:34.183 INFO Running the dyna-bindgen driver, generators: , manifest: Cargo.toml
Mar 04 03:23:34.183 INFO Analysing Project, manifest-path: /home/michael/Documents/dyna-bindgen/tests/dummy/Cargo.toml, context: invoke-rustc
Mar 04 03:23:34.602 INFO Invoking Rustc, target: Target(lib), version: 0.1.0, package: dummy, context: invoke-rustc
Mar 04 03:23:34.682 INFO Analysing crate with rustc, crate: dummy
Mar 04 03:23:34.682 INFO Running generators
Mar 04 03:23:34.683 INFO Bindings generated
```

[cbindgen]: https://github.com/eqrion/cbindgen
[dlopen]: https://linux.die.net/man/3/dlopen
[LoadLibrary]: https://msdn.microsoft.com/en-us/library/windows/desktop/ms684175(v=vs.85).aspx