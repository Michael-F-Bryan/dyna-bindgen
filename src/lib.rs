//! Generate FFI bindings for your program using dlopen/LoadLibrary for dynamic
//! linking.
//!
//! # Warning
//!
//! Because this uses the internals of both `cargo` and `rustc`, you will
//! need to use a nightly version of the compiler.

#![feature(rustc_private)]
#![feature(optin_builtin_traits)]
// #![deny(missing_docs, unsafe_code, missing_debug_implementations, missing_copy_implementations)]

extern crate getopts;
extern crate rustc;
extern crate rustc_driver;
extern crate syntax;

extern crate cargo;
extern crate failure;
#[macro_use]
extern crate failure_derive;
extern crate heck;
extern crate itertools;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate slog;

#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;

pub mod analysis;
pub mod generator;
mod driver;
pub mod utils;

pub use driver::Driver;
