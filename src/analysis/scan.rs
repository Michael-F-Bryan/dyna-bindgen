//! Analyse a crate's HIR, looking for things like functions, structs, and
//! constants which we'd typically include in a header file.

use std::marker::PhantomData;
use rustc::hir::{Crate, Generics, HirId, ImplItem, Item, Item_, TraitItem, Visibility};
use syntax::ast::Attribute;
use syntax::abi::Abi;
use rustc::hir::itemlikevisit::ItemLikeVisitor;
use slog::Logger;

/// Scan through a `hir::Crate`, looking for all types, constants, and functions
/// which belong in FFI bindings.
///
/// This will also try to check your crate to make sure any FFI bindings follow
/// best practice. We also try to detect typos where
///
/// [`hir::Crate`]: https://michael-f-bryan.github.io/rustc-internal-docs/rustc/hir/struct.Crate.html
pub fn scan<'hir>(hir: &'hir Crate, logger: &Logger) -> Bindings<'hir> {
    let logger = logger.new(o!());
    let mut scanner = Scanner::new(&logger, hir);

    hir.visit_all_item_likes(&mut scanner);
    let bindings = scanner.bindings;

    debug!(logger, "Finished analysing HIR";
        "functions" => bindings.functions.len(),
        "constants" => bindings.constants.len(),
        "statics" => bindings.statics.len(),
        "types" => bindings.types.len(),
        "warnings" => bindings.warnings.len());

    bindings
}

/// A distillation of what bindings should be generated for this crate.
#[derive(Debug, Default, Clone)]
pub struct Bindings<'hir> {
    pub functions: Vec<HirId>,
    pub types: Vec<HirId>,
    pub constants: Vec<HirId>,
    pub statics: Vec<HirId>,
    pub warnings: Vec<Warning>,
    /// A `HirId` is logically tied to the lifetime of the `hir::Crate` it
    /// points to, and the global `'tcx` which encapsulates the entire
    /// compilation process.
    ///
    /// Using the `HirId` after these lifetimes have ended will probably lead to
    /// runtime panics and sadness down the track so we statically ensure
    /// bindings can't live for too long with a `PhantomData<&'hir ()>`.
    ///
    /// In practice this will restrict `Bindings` to only live within a single
    /// invocation of `analysis::invoke_rustc_on_workspace()`.
    __crate_id: PhantomData<&'hir ()>,
}

impl<'hir> Bindings<'hir> {
    pub fn new() -> Bindings<'hir> {
        Bindings::default()
    }

    pub fn is_ok(&self) -> bool {
        self.warnings.is_empty()
    }
}

// A `HirId` is bound to a specific type context (`'tcx`) and the compiler  uses
// thread local storage a lot for efficient interning. It's a logical error for
// our bindings to escape the thread they were born in.
impl<'hir> !Send for Bindings<'hir> {}
impl<'hir> !Sync for Bindings<'hir> {}

struct Scanner<'hir> {
    logger: Logger,
    bindings: Bindings<'hir>,
}
impl<'hir> Scanner<'hir> {
    fn new(logger: &Logger, _krate: &'hir Crate) -> Scanner<'hir> {
        Scanner {
            logger: logger.clone(),
            bindings: Bindings::new(),
        }
    }

    fn handle_function_node(&mut self, item: &Item, abi: Abi, generics: &Generics) {
        let unmangled = is_no_mangle(&item.attrs);
        let is_generic = generics.is_type_parameterized();
        let uses_ffi_abi = abi != Abi::Rust;

        if !is_generic && uses_ffi_abi && unmangled {
            self.bindings.functions.push(item.hir_id);
        }

        // Try to emit helpful warnings and detect error cases

        if unmangled || uses_ffi_abi {
            // This *looks* like a FFI function, but the user either forgot the
            // #[no_mangle] attribute or didn't specify an ABI
            if !unmangled {
                self.bindings
                    .warnings
                    .push(Warning::ExternAbiWithoutNoMangle(item.hir_id));
            }
            if !uses_ffi_abi {
                self.bindings
                    .warnings
                    .push(Warning::NoMangleWithRustABI(item.hir_id));
            }
        }

        // the function is `extern "C"` and `#[no_mangle]`. Now we want to
        // apply some conventions (the visibility thing) or emit a warning
        // if it's accidentally been made generic.
        if unmangled && uses_ffi_abi {
            if item.vis != Visibility::Public {
                self.bindings
                    .warnings
                    .push(Warning::NonPublicFfiFunction(item.hir_id));
            }
            if is_generic {
                self.bindings
                    .warnings
                    .push(Warning::GenericFfiFunction(item.hir_id));
            }
        }
    }
}

impl<'hir> ItemLikeVisitor<'hir> for Scanner<'hir> {
    fn visit_item(&mut self, item: &'hir Item) {
        match item.node {
            Item_::ItemFn(_, _, _, abi, ref generics, _) => {
                trace!(self.logger, "Visiting function node";
                    "name" => &*item.name.as_str(),
                    "abi" => abi.name());
                self.handle_function_node(item, abi, generics);
            }
            Item_::ItemStatic(..) => {
                trace!(self.logger, "Visiting static";
                    "name" => &*item.name.as_str());
                if item.vis == Visibility::Public {
                    self.bindings.statics.push(item.hir_id);
                }
            }
            Item_::ItemConst(..) => {
                trace!(self.logger, "Visiting const";
                    "name" => &*item.name.as_str());
                if item.vis == Visibility::Public {
                    self.bindings.constants.push(item.hir_id);
                }
            }
            Item_::ItemStruct(_, ref generics) => {
                trace!(self.logger, "Visiting struct";
                    "name" => &*item.name.as_str());

                if is_repr(&item.attrs) && !generics.is_type_parameterized() {
                    self.bindings.types.push(item.hir_id);
                }
            }
            _ => {}
        }
    }

    fn visit_trait_item(&mut self, _trait_item: &'hir TraitItem) {}
    fn visit_impl_item(&mut self, _impl_item: &'hir ImplItem) {}
}

fn is_no_mangle(attrs: &[Attribute]) -> bool {
    attrs.into_iter().any(|attr| attr.check_name("no_mangle"))
}

fn is_repr(attrs: &[Attribute]) -> bool {
    attrs.into_iter().any(|attr| attr.check_name("repr"))
}

#[derive(Debug, Copy, Clone, PartialEq, Fail)]
pub enum Warning {
    #[fail(display = "Function is #[no_mangle] but uses a non-FFI-safe ABI")]
    NoMangleWithRustABI(HirId),
    #[fail(display = "Generic functions aren't FFI-safe")]
    GenericFfiFunction(HirId),
    #[fail(display = "Function has a FFI-safe ABI but no #[no_mangle] annotation")]
    ExternAbiWithoutNoMangle(HirId),
    #[fail(display = "FFI functions should be marked public")]
    NonPublicFfiFunction(HirId),
}
