use std::path::Path;
use std::cell::RefCell;
use std::rc::Rc;
use rustc_driver::driver::{CompileController, CompileState, PhaseController};
use rustc_driver::{self, Compilation, CompilerCalls};
use rustc::session::{CompileIncomplete, Session};
use cargo::core::Workspace;
use getopts::Matches;
use failure::{self, Error, ResultExt};
use slog::Logger;
use analysis;
use utils::{self, SyncResult};

/// Ask `rustc` to load a crate into memory and then inspect the compilation
/// state after it has been analysed.
pub fn invoke_rustc_on_workspace<P, F>(
    manifest_path: P,
    logger: &Logger,
    after_analysis: F,
) -> Result<(), Error>
where
    F: FnMut(&CompileState, &Workspace),
    P: AsRef<Path>,
{
    let logger = logger.new(o!());
    let manifest_path = manifest_path.as_ref().canonicalize()?;

    info!(logger, "Analysing Project"; 
        "manifest-path" => manifest_path.display());

    let cfg = utils::cargo_config()?;
    let ws = Workspace::new(&manifest_path, &cfg)
        .sync()
        .context("Couldn't load the workspace")?;

    let args = analysis::get_rustc_args(&ws, &cfg, &logger)
        .context("Unable to determine the arguments to pass to rustc")?;
    debug!(logger, "Determined rustc args"; "num-invocations" => args.len());

    let mut calls = Calls::new(after_analysis, ws);

    for &(ref args, ref package, ref target) in &args {
        info!(logger, "Invoking Rustc";
            "package" => package.name(),
            "version" => package.version().to_string(),
            "target" => target.to_string());
        trace!(logger, "Rustc Args"; "args" => args.join(" "));

        // by passing in `None` as the emitter destination, rustc will write
        // directly to stderr if any errors occur. This is annoying because it
        // means we can't inspect the error and don't have control over how it's
        // outputted, but it's probably the easiest way to do things for now.
        let (result, _sess) = rustc_driver::run_compiler(args, &mut calls, None, None);

        if let Err(CompileIncomplete::Errored(_)) = result {
            error!(logger, "Failed to compile the package"; "package" => package.name());
            return Err(failure::err_msg("Compilation Failed"));
        }
    }

    Ok(())
}

struct Calls<'cfg, F> {
    after_analysis: Rc<RefCell<F>>,
    workspace: Rc<Workspace<'cfg>>,
}

impl<'cfg, F> Calls<'cfg, F>
where
    F: FnMut(&CompileState, &Workspace),
{
    fn new(callback: F, ws: Workspace<'cfg>) -> Calls<'cfg, F> {
        Calls {
            after_analysis: Rc::new(RefCell::new(callback)),
            workspace: Rc::new(ws),
        }
    }
}

impl<'a, 'cfg: 'a, F> CompilerCalls<'a> for Calls<'cfg, F>
where
    F: FnMut(&CompileState, &Workspace<'cfg>) + 'a,
{
    fn build_controller(&mut self, _: &Session, _: &Matches) -> CompileController<'a> {
        let mut controller = CompileController::basic();

        let user_func = self.after_analysis.clone();
        let ws = self.workspace.clone();

        let cb = move |state: &mut CompileState| {
            let mut f = user_func.borrow_mut();
            (&mut *f)(&*state, &ws);
        };

        controller.after_hir_lowering = PhaseController {
            stop: Compilation::Stop,
            run_callback_on_error: true,
            callback: Box::new(cb),
        };

        controller
    }
}
