//! Crate analysis and collation of FFI functions, as well as the various type
//! declarations they depend on.
//!
//! If we want to generate bindings for a crate we first need to load it into
//! memory and scan through all the items,

mod arguments;
mod invoke;
mod scan;

pub use self::arguments::get_rustc_args;
pub use self::invoke::invoke_rustc_on_workspace;
pub use self::scan::{scan, Bindings, Warning};
