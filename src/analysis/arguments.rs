//! Instrument `cargo` to figure out what arguments to use when invoking rustc.

use std::sync::{Arc, Mutex};
use failure::{Error, ResultExt};
use slog::Logger;
use cargo::ops::{self, CompileMode, CompileOptions, Executor, Unit};
use cargo::util::{Config, ProcessBuilder};
use cargo::core::{PackageId, Target, Workspace};
use cargo::CargoResult;
use itertools::Itertools;

use utils::{self, SyncResult};

/// Instruments `cargo` to determine the arguments we should use when invoking
/// `rustc`.
///
/// We may invoke `rustc` multiple times (e.g. in a workspace), so multiple sets
/// of command-line arguments may be returned.
///
/// # Note
///
/// As a byproduct, because this calls `cargo build` on each package it should
/// ensure all dependencies are available and compiled.
pub fn get_rustc_args(
    ws: &Workspace,
    cfg: &Config,
    logger: &Logger,
) -> Result<Vec<(Vec<String>, PackageId, Target)>, Error> {
    let logger = logger.new(o!());
    debug!(logger, "Using cargo internals to find the right args for rustc";
        "manifest" => ws.current_manifest().display(),
        "virtual" => ws.is_virtual());

    let options = CompileOptions::default(cfg, CompileMode::Build);

    debug!(logger, "Running `cargo build`");

    let exec = Arc::new(ArgCollector::new(ws, &logger));
    ops::compile_with_exec(ws, &options, exec.clone())
        .sync()
        .context("Cargo build failed")?;

    let mut args = exec.args.lock().unwrap().clone();

    monkeypatch_arguments(&mut args)?;

    Ok(args)
}

/// Unfortunately it doesn't look like `cargo` is providing **all** the
/// information necessary to invoke `rustc` in the `args` part of its process
/// builder. Most notably we are missing:
///
/// - The leading `rustc` (argv[0])
/// - Sysroot information
fn monkeypatch_arguments(
    set_of_arguments: &mut [(Vec<String>, PackageId, Target)],
) -> Result<(), Error> {
    let sysroot = utils::get_sysroot();

    for &mut (ref mut args, _, _) in set_of_arguments {
        args.insert(0, String::from("rustc"));

        // FIXME: We shouldn't need to manually add the sysroot to link to `std`
        if let Some(ref sysroot) = sysroot {
            // if we were able to determine the sysroot, append it to the arguments.
            // If we couldn't well... at least we tried?
            args.push(String::from("--sysroot"));
            args.push(sysroot.display().to_string());
        }
    }

    Ok(())
}

#[derive(Debug)]
struct ArgCollector {
    args: Mutex<Vec<(Vec<String>, PackageId, Target)>>,
    logger: Logger,
    whitelist: Vec<PackageId>,
}

impl ArgCollector {
    fn new(ws: &Workspace, logger: &Logger) -> ArgCollector {
        ArgCollector {
            args: Default::default(),
            logger: logger.new(o!("operation" => "collecting-args")),
            whitelist: ws.members().map(|pkg| pkg.package_id().clone()).collect(),
        }
    }
}

impl Executor for ArgCollector {
    fn exec(&self, cmd: ProcessBuilder, id: &PackageId, target: &Target) -> CargoResult<()> {
        debug!(self.logger, "Cargo compiled a package";
            "package" => id.name(),
            "version" => id.version().to_string(),
            "target" => target.to_string());
        trace!(self.logger, "Arguments used";
            "args" => cmd.get_args().into_iter().map(|s| s.to_string_lossy()).join(" "));

        // we only really care about the arguments for the packages in the
        // current workspace
        if self.whitelist.contains(id) {
            let mut list_of_args = self.args.lock().unwrap();
            let args = cmd.get_args()
                .into_iter()
                .map(|s| s.to_string_lossy().into_owned())
                .collect();
            list_of_args.push((args, id.clone(), target.clone()));
        }

        let _ = cmd.exec_with_output()?;

        Ok(())
    }

    fn force_rebuild(&self, unit: &Unit) -> bool {
        let pkg_id = unit.pkg.package_id();
        self.whitelist.contains(pkg_id)
    }
}
