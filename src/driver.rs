use std::path::PathBuf;
use failure::{Error, ResultExt};
use slog::{Discard, Logger};
use itertools::Itertools;
use rustc_driver::driver::CompileState;
use cargo::core::Workspace;

use generator::{Generator, GeneratorCtx};
use analysis;

pub struct Driver {
    manifest: PathBuf,
    logger: Logger,
    generators: Vec<Box<Generator>>,
    output_dir: Option<PathBuf>,
}

impl Driver {
    pub fn new<P: Into<PathBuf>>(manifest: P) -> Driver {
        let logger = Logger::root(Discard, o!());
        Driver::new_with_logger(manifest, &logger)
    }

    pub fn new_with_logger<P: Into<PathBuf>>(manifest: P, logger: &Logger) -> Driver {
        Driver {
            manifest: manifest.into(),
            logger: logger.clone(),
            generators: Vec::new(),
            output_dir: None,
        }
    }

    pub fn with_output_dir<P: Into<PathBuf>>(&mut self, output_dir: P) -> &mut Self {
        self.output_dir = Some(output_dir.into());
        self
    }

    pub fn with_generator<G: Generator + 'static>(&mut self, gen: G) -> &mut Self {
        self.generators.push(Box::new(gen));
        self
    }

    pub fn run(&self) -> Result<(), Error> {
        info!(self.logger, "Running the dyna-bindgen driver";
            "manifest" => self.manifest.display(),
            "generators" => if self.generators.is_empty() { 
                String::from("<none>") 
            } else {
                self.generators.iter().map(|gen| gen.name()).join(",")
            });

        // we can't directly return an error from the closure, so we work around
        // it with a `Option<Error>` that'll be populated and returned when
        // anything goes wrong.
        let mut error = None;

        analysis::invoke_rustc_on_workspace(&self.manifest, &self.logger, |state, workspace| {
            if let Err(e) = self.gen(&*state, workspace) {
                error = Some(e);
            }
        })?;

        if let Some(err) = error {
            return Err(err);
        }

        info!(self.logger, "Bindings generated");

        Ok(())
    }

    fn gen(&self, state: &CompileState, ws: &Workspace) -> Result<(), Error> {
        info!(self.logger, "Analysing crate with rustc"; 
                "crate" => state.crate_name.as_ref());

        let hir = state.hir_crate.as_ref().unwrap();
        let bindings = analysis::scan(hir, &self.logger);

        info!(self.logger, "Running generators");
        let ctx = GeneratorCtx::from_compile_state(state);
        let target_dir = ws.target_dir().into_path_unlocked();

        for gen in &self.generators {
            let name = gen.name();
            debug!(self.logger, "Invoking Binding Generator";
                "generator" => name);
            gen.generate(&bindings, &target_dir, ctx)
                .with_context(|_| format!("The \"{}\" generator failed", name))?;
        }

        Ok(())
    }
}
