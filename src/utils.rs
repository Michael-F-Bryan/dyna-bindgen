use std::io;
use std::env;
use std::str;
use std::path::PathBuf;
use std::process::Command;
use cargo::Config;
use cargo::core::Shell;
use failure::{self, Error, ResultExt, SyncFailure};

pub fn get_sysroot() -> Option<PathBuf> {
    let output = Command::new("rustc")
        .arg("--print")
        .arg("sysroot")
        .output()
        .ok()?;

    if !output.status.success() {
        return None;
    }

    let s = str::from_utf8(&output.stdout).ok()?;
    Some(PathBuf::from(s.trim()))
}

/// Create a custom `Config` which uses a "black hole" shell so we don't spam
/// the user with any cargo output while determining rustc args.
pub fn cargo_config() -> Result<Config, Error> {
    let shell = Shell::from_write(Box::new(io::sink()));

    let cwd = env::current_dir().context("Unable to determine the current directory")?;
    let home =
        env::home_dir().ok_or_else(|| failure::err_msg("Unable to determine the home directory"))?;

    Ok(Config::new(shell, cwd, home))
}

pub trait SyncResult<T, E>: Sized {
    fn sync(self) -> Result<T, SyncFailure<E>>;
}

impl<T, E> SyncResult<T, E> for Result<T, E>
where
    E: ::std::error::Error + Send + 'static,
{
    fn sync(self) -> Result<T, SyncFailure<E>> {
        self.map_err(SyncFailure::new)
    }
}
