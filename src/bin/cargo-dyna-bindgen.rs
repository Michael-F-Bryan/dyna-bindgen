//! A "trampoline" binary which gives us access to `dyna-bindgen` as a `cargo`.
//!
//! Because we are hooking into the compiler we invoke the actual `dyna-bindgen`
//! binary as a "rustc wrapper" so `cargo` will give us the correct command-line
//! arguments. I'm sorry if that doesn't make too much sense straight away...
//! It's complicated.

#![feature(rustc_private)]

extern crate dyna_bindgen;
extern crate env_logger;
extern crate failure;
#[macro_use]
extern crate slog;
extern crate slog_async;
#[macro_use]
extern crate slog_derive;
extern crate slog_term;
#[macro_use]
extern crate structopt;

use std::env;
use std::process;
use std::path::Path;
use structopt::StructOpt;
use slog::{Drain, Level, Logger, PushFnValue};
use failure::Error;
use dyna_bindgen::Driver;
use dyna_bindgen::generator::{Delphi, Rust};

fn run(args: &Args, logger: &Logger) -> Result<(), Error> {
    info!(logger, "Starting dyna-bindgen"; args);

    let mut d = Driver::new_with_logger(args.manifest(), logger);
    args.load_generators(&mut d, logger)?;

    d.run()
}

fn main() {
    let args = parse_args();
    let logger = initialize_logging(args.verbosity);

    if let Err(e) = run(&args, &logger) {
        eprintln!("Error: {}", e);

        for cause in e.causes().skip(1) {
            eprintln!("\tCaused By: {}", cause);
        }

        let bt = e.backtrace().to_string();
        if !bt.trim().is_empty() {
            eprintln!("{}", bt);
        }

        process::exit(101);
    }
}

fn parse_args() -> Args {
    let mut raw_args: Vec<String> = env::args().collect();

    if raw_args.len() >= 2 && raw_args[1].contains("dyna-bindgen") {
        // we're being called as a cargo subcommand
        raw_args.remove(1);
    }

    Args::from_iter(raw_args)
}

#[derive(Debug, Clone, PartialEq, StructOpt, KV)]
#[structopt(name = "cargo-dyna-bindgen")]
pub struct Args {
    #[structopt(short = "v", long = "verbose", help = "Enable verbose output",
                parse(from_occurrences))]
    pub verbosity: u64,
    #[structopt(long = "manifest-path", help = "Path to the manifest to compile")]
    pub manifest_path: Option<String>,
    #[structopt(short = "g", long = "generator", help = "Binding generators to use",
                raw(possible_values = "&[\"rust\", \"delphi\"]"))]
    #[slog(skip)]
    pub generators: Vec<String>,
}

impl Args {
    pub fn manifest(&self) -> &Path {
        match self.manifest_path {
            Some(ref m) => Path::new(m),
            None => Path::new("Cargo.toml"),
        }
    }

    pub fn load_generators(&self, driver: &mut Driver, logger: &Logger) -> Result<(), Error> {
        for gen in &self.generators {
            match gen.to_lowercase().as_str() {
                "rust" => {
                    driver.with_generator(Rust::new_with_logger(logger));
                }
                "delphi" => {
                    driver.with_generator(Delphi::new_with_logger(logger));
                }
                _ => {
                    return Err(failure::err_msg(format!(
                        "The \"{}\" generator isn't supported",
                        gen
                    )))
                }
            }
        }

        Ok(())
    }
}

fn initialize_logging(verbosity: u64) -> Logger {
    env_logger::init();

    let decorator = slog_term::TermDecorator::new().stderr().build();

    let level = match verbosity {
        0 => Level::Warning,
        1 => Level::Info,
        2 => Level::Debug,
        _ => Level::Trace,
    };

    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::default(drain).fuse();

    let drain = drain.filter_level(level).fuse();

    if level > Level::Info {
        Logger::root(
            drain,
            o!("location" => PushFnValue(|r, ser| {
            ser.emit(format_args!("{}#{}", r.file(), r.line()))
        })),
        )
    } else {
        Logger::root(drain, o!())
    }
}
