//! A base pretty-printer that others can build on top of.
#![cfg_attr(not(test), allow(dead_code))]

use std::borrow::Borrow;
use std::io::{self, Write};
use failure::Error;

/// Convenience function for emitting a pretty-printed line.
macro_rules! emitln {
    ($obj:expr, $msg:expr) => {{
        $obj.emit_indents()?;
        $obj.pretty_printer().emit(&$msg)?;
        $obj.pretty_printer().newline()
    }};
    ($obj:expr, $fmt:expr, $( $arg:expr ),+) => {{
        use std::io::Write;
        let line = format!($fmt, $( $arg ),*);

        let pp = $obj.pretty_printer();
        pp.emit_indents()?;
        pp.write(line.as_bytes())?;
        pp.newline()
    }};
}

/// An arbitrary pretty printer.
///
/// Implement this trait to gain access to the `emitln!()` macro and a bunch of
/// convenient methods for pretty-printing text.
pub trait PP<W: Write> {
    fn pretty_printer(&mut self) -> &mut PrettyPrinter<W>;

    fn emit(&mut self, msg: &str) -> Result<(), Error> {
        self.pretty_printer().emit(msg)
    }
    fn indent(&mut self) {
        self.pretty_printer().indent();
    }
    fn dedent(&mut self) {
        self.pretty_printer().dedent();
    }
    fn emit_indents(&mut self) -> Result<(), Error> {
        self.pretty_printer().emit_indents()
    }
    fn newline(&mut self) -> Result<(), Error> {
        self.pretty_printer().newline()
    }
}

pub struct PrettyPrinter<W> {
    inner: W,
    indent_level: usize,
    indent: &'static str,
}

impl<W: Write> PrettyPrinter<W> {
    pub fn new(inner: W, indent: &'static str) -> PrettyPrinter<W> {
        PrettyPrinter {
            inner,
            indent_level: 0,
            indent,
        }
    }

    pub fn indent(&mut self) {
        self.indent_level += 1;
    }

    pub fn dedent(&mut self) {
        self.indent_level -= 1;
    }

    pub fn emit(&mut self, msg: &str) -> Result<(), Error> {
        self.inner.write_all(msg.as_bytes()).map_err(Error::from)
    }

    pub fn emit_indents(&mut self) -> Result<(), Error> {
        for _ in 0..self.indent_level {
            self.emit(self.indent)?;
        }

        Ok(())
    }

    pub fn newline(&mut self) -> Result<(), Error> {
        writeln!(self.inner).map_err(Error::from)
    }
}

impl PrettyPrinter<Vec<u8>> {
    pub fn in_memory(indent: &'static str) -> PrettyPrinter<Vec<u8>> {
        PrettyPrinter::new(Vec::new(), indent)
    }
}

impl<'a, W> PrettyPrinter<W>
where
    W: ToOwned<Owned = Vec<u8>>,
    Vec<u8>: Borrow<W>,
{
    /// Convenience function during testing to let you grab the body as a string
    /// if your writer is some sort of in-memory buffer.
    pub fn body_as_string(&self) -> String {
        let buffer: Vec<u8> = self.inner.to_owned();
        match String::from_utf8(buffer) {
            Ok(s) => s,
            Err(e) => panic!("The body should always be a string! {}", e),
        }
    }
}

impl<W: Write> Write for PrettyPrinter<W> {
    fn write(&mut self, buf: &[u8]) -> Result<usize, io::Error> {
        self.inner.write(buf)
    }
    fn flush(&mut self) -> Result<(), io::Error> {
        self.inner.flush()
    }
}
