use std::io::Write;
use std::path::{Path, PathBuf};
use std::fs::File;
use failure::{Error, ResultExt};
use slog::{Discard, Logger};
use heck::*;

use analysis::Bindings;
use generator::{Generator, GeneratorCtx, PrettyPrinter, PP};

/// Rust uses 4 spaces for indentation.
const INDENT: &str = "    ";

#[derive(Debug, Clone)]
pub struct Rust {
    logger: Logger,
    output_file: Option<PathBuf>,
}

impl Rust {
    /// Create a new `Rust` bindings generator.
    pub fn new() -> Rust {
        let logger = Logger::root(Discard, o!());
        Rust::new_with_logger(&logger)
    }

    /// Create a new `Rust` generator using a specific logger.
    pub fn new_with_logger(logger: &Logger) -> Rust {
        Rust {
            logger: logger.new(o!("generator" => "rust")),
            output_file: None,
        }
    }

    pub fn with_output_file<P: Into<PathBuf>>(&mut self, output_file: P) -> &mut Self {
        self.output_file = Some(output_file.into());
        self
    }
}

impl Generator for Rust {
    fn generate(
        &self,
        bindings: &Bindings,
        output_dir: &Path,
        ctx: GeneratorCtx,
    ) -> Result<(), Error> {
        let output_file = match self.output_file {
            Some(ref out) => out.clone(),
            None => output_dir.join(ctx.crate_name).with_extension("rs"),
        };

        info!(self.logger, "Running the Rust generator"; 
            "output-file" => output_file.display(),
            "crate" => ctx.crate_name);

        let f = File::create(&output_file).context("Unable to open the output file")?;
        Printer::new(f, &self.logger, bindings.clone(), ctx).write_bindings()?;

        Ok(())
    }

    fn name(&self) -> &str {
        "rust"
    }
}

impl Default for Rust {
    fn default() -> Rust {
        Rust::new()
    }
}

/// A Rust-specific printer.
struct Printer<'a, 'tcx: 'a, W> {
    inner: PrettyPrinter<W>,
    logger: Logger,
    bindings: Bindings<'a>,
    ctx: GeneratorCtx<'a, 'tcx>,
    struct_name: String,
}

impl<'a, 'tcx: 'a, W: Write> Printer<'a, 'tcx, W> {
    fn write_bindings(&mut self) -> Result<(), Error> {
        trace!(self.logger, "Writing Standard Imports");
        self.emit_imports()?;
        self.newline()?;

        if !self.bindings.constants.is_empty() {
            trace!(self.logger, "Writing Constants";
                "number" => self.bindings.constants.len());
            self.emit_constants()?;
            self.newline()?;
        }

        if !self.bindings.types.is_empty() {
            trace!(self.logger, "Writing Type Declarations"; 
                "number" => self.bindings.types.len());
            self.emit_types()?;
            self.newline()?;
        }

        self.emit_library_struct()?;

        Ok(())
    }

    fn emit_library_struct(&mut self) -> Result<(), Error> {
        trace!(self.logger, "Writing library struct declaration");
        self.emit_library_decl()?;
        self.newline()?;

        trace!(self.logger, "Writing library struct impl");
        self.emit_library_impl()?;
        self.newline()?;

        trace!(self.logger, "Writing the vtable decl");
        self.emit_vtable_decl()?;
        self.newline()?;

        trace!(self.logger, "Writing the vtable impl block");
        self.emit_vtable_impl()?;

        Ok(())
    }

    fn emit_vtable_decl(&mut self) -> Result<(), Error> {
        emitln!(
            self,
            "/// A vtable containing pointers to the various foreign functions."
        )?;
        emitln!(self, "struct Vtable {")?;
        self.indent();

        self.dedent();
        emitln!(self, "}")?;

        Ok(())
    }

    fn emit_vtable_impl(&mut self) -> Result<(), Error> {
        emitln!(self, "impl Vtable {")?;
        self.indent();

        emitln!(
            self,
            "fn from_library(lib: &libloading::Library) -> libloading::Result<Vtable> {"
        )?;
        self.indent();
        // TODO: extract all the function pointers
        emitln!(self, "Ok(Vtable {})")?;
        self.dedent();
        emitln!(self, "}")?;

        self.dedent();
        emitln!(self, "}")?;

        Ok(())
    }

    fn emit_imports(&mut self) -> Result<(), Error> {
        emitln!(self, "use std::ffi::OsStr;")?;
        emitln!(self, "use libloading;")?;
        Ok(())
    }

    fn emit_constants(&mut self) -> Result<(), Error> {
        Ok(())
    }

    fn emit_types(&mut self) -> Result<(), Error> {
        Ok(())
    }

    fn emit_library_decl(&mut self) -> Result<(), Error> {
        emitln!(
            self,
            "/// Dynamic FFI bindings to the `{}` crate.",
            self.ctx.crate_name
        )?;
        emitln!(self, "pub struct {} {{", self.struct_name)?;
        self.indent();

        emitln!(self, "lib: libloading::Library,")?;
        emitln!(self, "vtable: Vtable,")?;

        // TODO: iterate over function bindings

        self.dedent();
        emitln!(self, "}")?;
        self.newline()?;

        Ok(())
    }

    fn emit_library_impl(&mut self) -> Result<(), Error> {
        emitln!(self, "impl {} {{", self.struct_name)?;
        self.indent();

        trace!(self.logger, "Writing the constructor");
        self.emit_constructor()?;

        self.dedent();
        emitln!(self, "}")?;

        Ok(())
    }

    fn emit_constructor(&mut self) -> Result<(), Error> {
        emitln!(
            self,
            "pub fn new<P: AsRef<OsStr>>(filename: P) -> libloading::Result<Self> {"
        )?;
        self.indent();
        emitln!(self, "let lib = libloading::Library::new(filename)?;")?;
        emitln!(self, "let vtable = Vtable::from_library(&lib)?;")?;
        emitln!(self, "Ok({} {{ lib, vtable }})", self.struct_name)?;
        self.dedent();
        emitln!(self, "}")?;
        Ok(())
    }
}

impl<'a, 'tcx: 'a, W: Write> Printer<'a, 'tcx, W> {
    pub fn new(
        writer: W,
        logger: &Logger,
        bindings: Bindings<'a>,
        ctx: GeneratorCtx<'a, 'tcx>,
    ) -> Printer<'a, 'tcx, W> {
        let inner = PrettyPrinter::new(writer, INDENT);
        let logger = logger.clone();
        let struct_name = ctx.crate_name.to_camel_case();

        Printer {
            inner,
            logger,
            bindings,
            ctx,
            struct_name,
        }
    }
}

impl<'a, 'tcx: 'a, W: Write> PP<W> for Printer<'a, 'tcx, W> {
    fn pretty_printer(&mut self) -> &mut PrettyPrinter<W> {
        &mut self.inner
    }
}
