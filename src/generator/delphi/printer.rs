use std::io::Write;
use failure::Error;
use slog::{Discard, Logger};
use itertools::Itertools;
use heck::*;

use super::ast::{Function, Ty, Unit};
use generator::errors::InvalidIntegerType;
use generator::{PrettyPrinter, PP};

/// Delphi uses two spaces for indentation.
const INDENT: &str = "  ";

/// A really basic Delphi pretty-printer.
pub struct Printer<W>(PrettyPrinter<W>, Logger);

impl<W: Write> Printer<W> {
    #[allow(dead_code)]
    pub fn new(inner: W) -> Printer<W> {
        let logger = Logger::root(Discard, o!());
        Printer::new_with_logger(inner, &logger)
    }

    pub fn new_with_logger(inner: W, logger: &Logger) -> Printer<W> {
        Printer(PrettyPrinter::new(inner, INDENT), logger.clone())
    }

    pub fn emit_unit(&mut self, unit: &Unit) -> Result<(), Error> {
        trace!(self.1, "Writing the unit line"; "name" => &unit.name);
        self.emit_unit_line(&unit.name)?;
        self.newline()?;

        trace!(self.1, "Writing the interface section");
        self.interface_section(unit)?;
        self.newline()?;

        trace!(self.1, "Writing the implementation section");
        self.implementation_section(unit)?;
        self.newline()?;

        trace!(self.1, "Writing the \"end.\"");
        self.emit_end()?;

        Ok(())
    }

    fn interface_section(&mut self, unit: &Unit) -> Result<(), Error> {
        emitln!(self, "interface")?;
        self.newline()?;

        self.emit_uses(&unit.uses)?;

        emitln!(self, "type")?;
        self.indent();
        self.newline()?;

        trace!(self.1, "Generating boilerplate classes");
        emitln!(self, "EDllLoadError = class(Exception);")?;
        self.newline()?;

        trace!(self.1, "Writing the declaration for the main library class");
        self.emit_library_decl(unit)?;

        self.dedent();

        Ok(())
    }

    fn emit_uses(&mut self, uses: &[String]) -> Result<(), Error> {
        match uses.len() {
            0 => return Ok(()),
            1 => {
                emitln!(self, "uses {};", &uses[0])?;
            }
            _ => {
                emitln!(self, "uses {},", &uses[0])?;
                self.indent();

                for thing in uses.iter().skip(1).dropping_back(1) {
                    emitln!(self, "{},", thing)?;
                }

                emitln!(self, "{};", uses[uses.len() - 1])?;
                self.dedent();
            }
        }

        self.newline()?;
        Ok(())
    }

    fn emit_library_decl(&mut self, unit: &Unit) -> Result<(), Error> {
        emitln!(self, "T{} = class", unit.name.to_camel_case())?;

        // private section
        emitln!(self, "private")?;
        self.indent();
        emitln!(self, "handle: NativeUint;")?;
        self.newline()?;
        self.dedent();

        // public section
        emitln!(self, "public")?;
        self.indent();

        self.emit_function_fields(&unit.functions)?;
        self.newline()?;

        emitln!(self, "constructor Create(const lib: String);")?;
        emitln!(self, "destructor Destroy; override;")?;
        self.dedent();

        emitln!(self, "end;")?;

        Ok(())
    }

    fn emit_function_fields(&mut self, functions: &[Function]) -> Result<(), Error> {
        for func in functions {
            self.emit_indents()?;
            self.emit_function_attribute(func)?;
            self.newline()?;
        }

        Ok(())
    }

    fn implementation_section(&mut self, unit: &Unit) -> Result<(), Error> {
        emitln!(self, "implementation")?;
        self.newline()?;

        emitln!(self, "{{ T{} }}", unit.name.to_camel_case())?;
        self.newline()?;

        trace!(self.1, "Generating the class constructor");
        self.emit_constructor(unit)?;
        self.newline()?;

        trace!(self.1, "Generating the class destructor");
        self.emit_destructor(&unit.name)?;

        Ok(())
    }

    fn emit_constructor(&mut self, unit: &Unit) -> Result<(), Error> {
        emitln!(
            self,
            "constructor T{}.Create(const lib: String);",
            unit.name.to_camel_case()
        )?;

        emitln!(self, "begin")?;
        self.indent();

        emitln!(self, "if not FileExists(lib) then")?;
        self.indent();
        emitln!(
            self,
            "raise EDllLoadError.CreateFmt('%s doesn''t exist', [lib]);"
        )?;
        self.dedent();
        self.newline()?;

        emitln!(self, "handle := LoadLibrary(PWideChar(lib));")?;
        emitln!(self, "if handle = 0 then")?;
        self.indent();
        emitln!(
            self,
            "raise EDllLoadError.CreateFmt('Unable to load %s', [lib]);"
        )?;
        self.dedent();
        self.newline()?;

        for function in &unit.functions {
            emitln!(
                self,
                "@{0} := GetProcAddress(handle, PWideChar('{0}'));",
                function.name
            )?;
            emitln!(self, "if @{} = nil then", function.name)?;
            self.indent();
            emitln!(
                self,
                "raise EDllLoadError.Create('Couldn''t find the \"{}\" function');",
                function.name
            )?;
            self.dedent();
        }

        self.dedent();
        emitln!(self, "end;")?;

        Ok(())
    }

    fn emit_destructor(&mut self, name: &str) -> Result<(), Error> {
        emitln!(self, "destructor T{}.Destroy;", name.to_camel_case())?;

        emitln!(self, "begin")?;
        self.indent();

        emitln!(self, "if handle <> 0 then")?;
        self.indent();
        emitln!(self, "FreeLibrary(handle);")?;
        self.dedent();

        self.newline()?;
        emitln!(self, "inherited;")?;

        self.dedent();
        emitln!(self, "end;")?;

        Ok(())
    }

    fn emit_unit_line(&mut self, name: &str) -> Result<(), Error> {
        emitln!(self, "unit {};", name)
    }

    fn emit_end(&mut self) -> Result<(), Error> {
        emitln!(self, "end.")
    }

    fn emit_ty(&mut self, ty: &Ty) -> Result<(), Error> {
        match *ty {
            Ty::Boolean => self.emit("Boolean"),
            Ty::Integer { size, signed } => self.emit_integer(size, signed),
            Ty::Float => self.emit("Float"),
            Ty::Double => self.emit("Double"),
            Ty::Utf16String => self.emit("PChar"),
            Ty::Utf8String => self.emit("PAnsiChar"),
            Ty::OpaquePointer(ref name) => {
                self.emit("P")?;
                self.emit(name)
            }
            _ => unimplemented!(),
        }
    }

    fn emit_integer(&mut self, size: usize, signed: bool) -> Result<(), Error> {
        match (size, signed) {
            (1, true) => self.emit("ShortInt"),
            (1, false) => self.emit("Byte"),
            (2, true) => self.emit("SmallInt"),
            (2, false) => self.emit("Word"),
            (4, true) => self.emit("Integer"),
            (4, false) => self.emit("Cardinal"),
            (8, true) => self.emit("Int64"),
            (8, false) => self.emit("Uint64"),
            _ => Err(InvalidIntegerType::new(size, signed).into()),
        }
    }

    fn emit_function_attribute(&mut self, f: &Function) -> Result<(), Error> {
        self.emit(&f.name)?;
        self.emit(": ")?;

        if f.is_procedure() {
            self.emit("procedure")?;
        } else {
            self.emit("function")?;
        }

        if !f.signature.inputs.is_empty() {
            self.emit("(")?;

            let first_arg = &f.signature.inputs[0];
            self.emit(&first_arg.0)?;
            self.emit(": ")?;
            self.emit_ty(&first_arg.1)?;

            for &(ref arg, ref ty) in f.signature.inputs.iter().skip(1) {
                self.emit(", ")?;
                self.emit(arg)?;
                self.emit(": ")?;
                self.emit_ty(ty)?;
            }

            self.emit(")")?;
        }

        if let Some(ref out_ty) = f.signature.output {
            self.emit(": ")?;
            self.emit_ty(out_ty)?;
        }

        self.emit(";")?;
        Ok(())
    }
}

#[cfg(test)]
impl Printer<Vec<u8>> {
    fn in_memory() -> Printer<Vec<u8>> {
        Printer(
            PrettyPrinter::in_memory(INDENT),
            Logger::root(Discard, o!()),
        )
    }
}

impl<W: Write> PP<W> for Printer<W> {
    fn pretty_printer(&mut self) -> &mut PrettyPrinter<W> {
        &mut self.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::ast::FnDecl;

    macro_rules! compare_text {
        ($left:expr, $right:expr) => {
            if $left != $right {
                let left: Vec<_> = $left.lines().collect();
                let right: Vec<_> = $right.lines().collect();
                assert_eq!(left, right);
            }
        };
    }

    #[test]
    fn empty_unit() {
        let unit = Unit::new("empty");
        let should_be = "\
unit empty;

interface

uses System.Classes,
  System.SysUtils,
  Windows;

type

  EDllLoadError = class(Exception);

  TEmpty = class
  private
    handle: NativeUint;

  public

    constructor Create(const lib: String);
    destructor Destroy; override;
  end;

implementation

{ TEmpty }

constructor TEmpty.Create(const lib: String);
begin
  if not FileExists(lib) then
    raise EDllLoadError.CreateFmt('%s doesn''t exist', [lib]);

  handle := LoadLibrary(PWideChar(lib));
  if handle = 0 then
    raise EDllLoadError.CreateFmt('Unable to load %s', [lib]);

end;

destructor TEmpty.Destroy;
begin
  if handle <> 0 then
    FreeLibrary(handle);

  inherited;
end;

end.
";

        let mut p = Printer::new(Vec::new());
        p.emit_unit(&unit).unwrap();

        let got = p.0.body_as_string();
        compare_text!(got, should_be);
    }

    #[test]
    fn emit_basic_types() {
        let inputs = vec![
            (Ty::Boolean, "Boolean"),
            (Ty::integer(1, true), "ShortInt"),
            (Ty::integer(1, false), "Byte"),
            (Ty::integer(2, true), "SmallInt"),
            (Ty::integer(2, false), "Word"),
            (Ty::integer(4, true), "Integer"),
            (Ty::integer(4, false), "Cardinal"),
            (Ty::integer(8, true), "Int64"),
            (Ty::integer(8, false), "Uint64"),
            (Ty::Float, "Float"),
            (Ty::Double, "Double"),
            (Ty::Utf8String, "PAnsiChar"),
            (Ty::Utf16String, "PChar"),
            (Ty::OpaquePointer(String::from("Logger")), "PLogger"),
        ];

        for (ty, should_be) in inputs {
            let mut p = Printer::new(Vec::new());
            p.emit_ty(&ty).unwrap();

            let got = p.0.body_as_string();
            compare_text!(got, should_be);
        }
    }

    #[test]
    fn generate_the_destructor() {
        let should_be = "\
destructor TEmpty.Destroy;
begin
  if handle <> 0 then
    FreeLibrary(handle);

  inherited;
end;
";

        let mut p = Printer::in_memory();
        p.emit_destructor("empty").unwrap();

        let got = p.0.body_as_string();
        compare_text!(got, should_be);
    }

    #[test]
    fn generate_the_constructor() {
        let should_be = "\
constructor TLogs.Create(const lib: String);
begin
  if not FileExists(lib) then
    raise EDllLoadError.CreateFmt('%s doesn''t exist', [lib]);

  handle := LoadLibrary(PWideChar(lib));
  if handle = 0 then
    raise EDllLoadError.CreateFmt('Unable to load %s', [lib]);

  @logger_new := GetProcAddress(handle, PWideChar('logger_new'));
  if @logger_new = nil then
    raise EDllLoadError.Create('Couldn''t find the \"logger_new\" function');
end;
";
        let mut unit = Unit::new("Logs");

        let args = vec![(String::from("verbosity"), Ty::integer(4, true))];
        let decl = FnDecl::function(args, Ty::OpaquePointer(String::from("Logger")));
        let func = Function::new("logger_new", decl);
        unit.functions.push(func);

        let mut p = Printer::in_memory();
        p.emit_constructor(&unit).unwrap();

        let got = p.0.body_as_string();
        compare_text!(got, should_be);
    }

    #[test]
    fn generate_a_function_pointer_attribute() {
        let should_be = "logger_new: function(verbosity: Integer): PLogger;";

        let args = vec![(String::from("verbosity"), Ty::integer(4, true))];
        let decl = FnDecl::function(args, Ty::OpaquePointer(String::from("Logger")));
        let func = Function::new("logger_new", decl);

        let mut p = Printer::in_memory();
        p.emit_function_attribute(&func).unwrap();

        let got = p.0.body_as_string();
        compare_text!(got, should_be);
    }

    #[test]
    fn generate_library_class_declaration() {
        let mut unit = Unit::new("empty");

        let args = vec![(String::from("verbosity"), Ty::integer(4, true))];
        let decl = FnDecl::function(args, Ty::OpaquePointer(String::from("Logger")));
        let func = Function::new("logger_new", decl);
        unit.functions.push(func);

        let should_be = "\
TEmpty = class
private
  handle: NativeUint;

public
  logger_new: function(verbosity: Integer): PLogger;

  constructor Create(const lib: String);
  destructor Destroy; override;
end;
";

        let mut p = Printer::in_memory();
        p.emit_library_decl(&unit).unwrap();

        let got = p.0.body_as_string();
        compare_text!(got, should_be);
    }

    #[test]
    fn write_out_uses() {
        let inputs: Vec<(&[&str], &str)> = vec![
            (&[], ""),
            (&["first"], "uses first;\n\n"),
            (&["first", "second"], "uses first,\n  second;\n\n"),
            (
                &["first", "second", "third"],
                "uses first,\n  second,\n  third;\n\n",
            ),
        ];

        for (src, should_be) in inputs {
            let src: Vec<String> = src.into_iter().map(|s| s.to_string()).collect();

            let mut p = Printer::in_memory();
            p.emit_uses(&src).unwrap();

            let got = p.0.body_as_string();
            compare_text!(got, should_be);
        }
    }
}
