mod ast;
mod printer;

use std::path::Path;
use std::fs::File;
use slog::{Discard, Logger};
use failure::{Error, ResultExt};

use generator::{Generator, GeneratorCtx};
use analysis::Bindings;
use self::printer::Printer;
use self::ast::Unit;

/// A type for generating a Delphi unit which will interface with your library
/// by loading it with winapi's [`LoadLibrary()`] function.
///
/// [`LoadLibrary()`]: https://msdn.microsoft.com/en-us/library/windows/desktop/ms684175(v=vs.85).aspx
#[derive(Debug, Clone)]
pub struct Delphi {
    logger: Logger,
}

impl Delphi {
    /// Create a new `Delphi` bindings generator.
    pub fn new() -> Delphi {
        let logger = Logger::root(Discard, o!());
        Delphi::new_with_logger(&logger)
    }

    /// Create a new `Delphi` generator using a specific logger.
    pub fn new_with_logger(logger: &Logger) -> Delphi {
        Delphi {
            logger: logger.new(o!("generator" => "delphi")),
        }
    }

    fn resolve_unit(&self, _bindings: &Bindings, ctx: GeneratorCtx) -> Result<Unit, Error> {
        debug!(self.logger, "Translating to Delphi types");

        let unit = Unit::new(ctx.crate_name);

        Ok(unit)
    }
}

impl Generator for Delphi {
    fn generate(
        &self,
        bindings: &Bindings,
        output_dir: &Path,
        ctx: GeneratorCtx,
    ) -> Result<(), Error> {
        let output_file = output_dir.join(ctx.crate_name).with_extension("pas");

        info!(self.logger, "Started Delphi Generator"; 
            "output-file" => output_file.display(),
            "crate" => ctx.crate_name);

        let f = File::create(&output_file).context("Unable to open the output file")?;
        let mut printer = Printer::new_with_logger(f, &self.logger);

        let unit = self.resolve_unit(bindings, ctx)?;
        printer.emit_unit(&unit)?;

        Ok(())
    }

    fn name(&self) -> &str {
        "delphi"
    }
}

impl Default for Delphi {
    fn default() -> Delphi {
        Delphi::new()
    }
}
