#![allow(dead_code)]

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Unit {
    pub name: String,
    pub uses: Vec<String>,
    pub functions: Vec<Function>,
}

const DEFAULT_USES: &[&str] = &["System.Classes", "System.SysUtils", "Windows"];

impl Unit {
    pub fn new<S: Into<String>>(name: S) -> Unit {
        Unit {
            name: name.into(),
            uses: DEFAULT_USES.into_iter().map(|s| s.to_string()).collect(),
            functions: Vec::new(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Const {
    Integer(i64),
    Float(f64),
    String(String),
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Record {
    pub name: String,
    pub docs: Option<String>,
    pub fields: Vec<Field>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Field {
    pub name: String,
    pub docs: Option<String>,
    pub ty: Ty,
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub enum Ty {
    Boolean,
    /// An integer with a fixed size.
    Integer {
        size: usize,
        signed: bool,
    },
    /// A 32-bit floating point number.
    Float,
    /// A 64-bit floating point number.
    Double,
    /// A pointer to a null-terminated UTF-8 string.
    Utf8String,
    /// A pointer to a null-terminated UTF-16 string.
    Utf16String,
    /// A function declaration.
    Func(Box<FnDecl>),
    /// A pointer to an unknown type (effectively C's `void*` with some type
    /// safety).
    OpaquePointer(String),
}

impl Ty {
    pub fn integer(size: usize, signed: bool) -> Ty {
        Ty::Integer { size, signed }
    }
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub struct Function {
    pub name: String,
    pub docs: Option<String>,
    pub signature: FnDecl,
}

impl Function {
    pub fn new<S: Into<String>>(name: S, decl: FnDecl) -> Function {
        Function {
            name: name.into(),
            docs: None,
            signature: decl,
        }
    }

    pub fn is_procedure(&self) -> bool {
        self.signature.is_procedure()
    }
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub struct FnDecl {
    pub inputs: Vec<(String, Ty)>,
    pub output: Option<Ty>,
}

impl FnDecl {
    pub fn function(inputs: Vec<(String, Ty)>, output: Ty) -> FnDecl {
        FnDecl {
            inputs,
            output: Some(output),
        }
    }

    pub fn procedure(inputs: Vec<(String, Ty)>) -> FnDecl {
        FnDecl {
            inputs,
            output: None,
        }
    }

    pub fn is_procedure(&self) -> bool {
        self.output.is_none()
    }
}
