//! Binding Generators.
//!
//! Some tips for implementing generators:
//!
//! - It's usually helpful to create your own IR for your language because it
//!   - is easier to test
//!   - is easier to maintain
//!   - leads to more readable code and a cleaner implementation
//! - Rustc's internals aren't guaranteed to be stable so the less you use
//!   internal APIs the less likely things are to break when a new nightly is
//!   released.

#[macro_use]
mod pretty_printer;
mod delphi;
mod rust;
mod errors;

pub use self::delphi::Delphi;
pub use self::rust::Rust;
pub use self::errors::*;
pub use self::pretty_printer::{PrettyPrinter, PP};

use std::path::Path;
use rustc::hir;
use rustc::session::Session;
use rustc::hir::map::Map;
use rustc_driver::driver::CompileState;
use rustc::ty::Resolutions;
use syntax::ast;
use failure::Error;
use analysis::Bindings;

/// A binding generator.
pub trait Generator {
    /// Generate the bindings.
    fn generate(
        &self,
        bindings: &Bindings,
        output_dir: &Path,
        outctx: GeneratorCtx,
    ) -> Result<(), Error>;

    /// The generator's name.
    fn name(&self) -> &str;
}

/// Context around the current compilation.
#[derive(Clone, Copy)]
pub struct GeneratorCtx<'a, 'tcx: 'a> {
    /// The parsed AST.
    pub crate_ast: &'a ast::Crate,
    /// The HIR representation for the crate who's bindings are to be generated.
    pub hir_crate: &'a hir::Crate,
    /// A convenient map which allows you to convert between `HirId` (HIR nodes),
    /// `NodeId` (AST nodes), and `DefId`'s (an ID used during type checking).
    pub hir_map: &'a Map<'tcx>,
    /// The resolved types in this crate.
    pub resolutions: &'a Resolutions,
    /// The current compilation session. Usually used when emitting diagnostics
    /// to the user.
    pub session: &'tcx Session,
    /// Name of the crate being compiled.
    pub crate_name: &'a str,
    __non_exhaustive: (),
}

impl<'a, 'tcx: 'a> GeneratorCtx<'a, 'tcx> {
    pub(crate) fn from_compile_state(state: &CompileState<'a, 'tcx>) -> Self {
        GeneratorCtx {
            resolutions: state
                .resolutions
                .expect("We should always be able to access the type resolutions. This is a bug."),
            hir_crate: state
                .hir_crate
                .expect("We should always be able to access the HIR. This is a bug."),
            hir_map: state
                .hir_map
                .expect("We should always be able to access the HIR Map. This is a bug."),
            crate_ast: state
                .expanded_crate
                .expect("We should always be able to access the crate's AST. This is a bug."),
            session: state.session,
            crate_name: state
                .crate_name
                .expect("We should always be able to access the crate's name. This is a bug."),
            __non_exhaustive: (),
        }
    }
}
