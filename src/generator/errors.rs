use std::fmt::{self, Display, Formatter};

/// A specific integer type isn't valid (e.g. there's no such thing as a 7-byte
/// signed integer).
#[derive(Debug, Copy, Clone, PartialEq, Fail)]
pub struct InvalidIntegerType {
    /// The integer's size in bytes.
    pub size: usize,
    /// Whether the integer is sized or not.
    pub signed: bool,
}

impl InvalidIntegerType {
    pub fn new(size: usize, signed: bool) -> InvalidIntegerType {
        InvalidIntegerType { size, signed }
    }
}

impl Display for InvalidIntegerType {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        if self.signed {
            write!(f, "Signed")?;
        } else {
            write!(f, "Unsigned")?;
        }

        write!(f, " integer of size {} is invalid", self.size)
    }
}
