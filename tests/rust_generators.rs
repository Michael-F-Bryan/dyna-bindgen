#![feature(rustc_private)]

extern crate cargo;
extern crate dyna_bindgen;
extern crate env_logger;
extern crate failure;
#[macro_use]
extern crate slog;
extern crate slog_term;
extern crate tempdir;

use std::path::{Path, PathBuf};
use std::io::Write;
use std::fs::{File, OpenOptions};
use std::sync::Mutex;
use tempdir::TempDir;
use cargo::ops::{self, CompileMode, CompileOptions, NewOptions};
use cargo::core::Workspace;
use cargo::Config;
use dyna_bindgen::Driver;
use dyna_bindgen::generator::Rust;
use dyna_bindgen::utils::SyncResult;
use slog::{Discard, Drain, Logger};
use failure::{Error, SyncFailure};

/// Disable logging during tests altogether.
const QUIET: bool = true;

#[test]
fn run_the_rust_generator() {
    let proj = match TempCrate::new() {
        Ok(p) => p,
        Err(e) => {
            eprintln!("Error: {}", e);

            for cause in e.causes().skip(1) {
                eprintln!("\tCaused By: {}", cause);
            }
            eprintln!("{}", e.backtrace());

            panic!("{}", e);
        }
    };
    let logger = test_logger();
    let dummy = dummy_crate().join("Cargo.toml");

    let output_file = proj.bindings_rs();
    let mut rust = Rust::new_with_logger(&logger);
    rust.with_output_file(&output_file);

    let mut driver = Driver::new_with_logger(dummy, &logger);
    driver.with_generator(rust).run().unwrap();

    proj.compile().unwrap();

    assert!(output_file.exists());
}

fn dummy_crate() -> PathBuf {
    PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("tests/dummy")
}

fn test_logger() -> Logger {
    // When trying to figure out the args for rustc, cargo will try to run each job
    // in parallel. As a result, our `slog_term::TestStdoutWriter` is sometimes
    // written to from another thread. This is annoying because the test runner can
    // only capture (and suppress) output when it's generated from the main thread,
    // which means passing tests still spam the user.
    if QUIET {
        Logger::root(Discard, o!())
    } else {
        env_logger::try_init().ok();
        let decorator = slog_term::PlainDecorator::new(slog_term::TestStdoutWriter);
        let drain = slog_term::CompactFormat::new(decorator).build().fuse();
        let drain = Mutex::new(drain).fuse();

        Logger::root(drain, o!())
    }
}

#[derive(Debug)]
pub struct TempCrate(TempDir);

impl TempCrate {
    pub fn new() -> Result<TempCrate, Error> {
        let dir = TempDir::new("dyna-bindgen")?;
        let this = TempCrate(dir);
        this.init()?;
        Ok(this)
    }

    pub fn bindings_rs(&self) -> PathBuf {
        self.path().join("src").join("bindings.rs")
    }

    /// Run `cargo init` in the directory.
    fn init(&self) -> Result<(), Error> {
        let cfg = Config::default().sync()?;
        let opts = NewOptions::new(
            None,
            false,
            true,
            self.path().to_str().unwrap(),
            Some("dyn_test"),
        );
        ops::init(&opts, &cfg).sync()?;

        let lib_rs = self.path().join("src").join("lib.rs");

        let mut f = File::create(&lib_rs)?;
        writeln!(f, "extern crate libloading;")?;
        writeln!(f, "pub mod bindings;")?;

        let config_toml = self.path().join("Cargo.toml");
        let mut f = OpenOptions::new()
            .write(true)
            .append(true)
            .create(false)
            .open(&config_toml)?;
        writeln!(f, "libloading = \"0.5.0\"")?;

        Ok(())
    }

    pub fn path(&self) -> &Path {
        self.0.path()
    }

    /// Run `cargo build` and make sure we can compile the crate.
    pub fn compile(&self) -> Result<(), Error> {
        let mut cfg = Config::default().map_err(SyncFailure::new)?;
        cfg.configure(
            0,
            Some(true),
            &Some(String::from("never")),
            false,
            false,
            &[],
        ).map_err(SyncFailure::new)?;

        let manifest_path = self.path().join("Cargo.toml");

        let ws = Workspace::new(&manifest_path, &cfg).map_err(SyncFailure::new)?;
        let options = CompileOptions::default(&cfg, CompileMode::Build);

        ops::compile(&ws, &options).map_err(SyncFailure::new)?;

        Ok(())
    }

    pub fn leak(self) -> PathBuf {
        self.0.into_path()
    }
}
