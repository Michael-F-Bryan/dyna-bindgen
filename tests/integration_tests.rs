#![feature(rustc_private)]

extern crate cargo;
extern crate dyna_bindgen;
extern crate env_logger;
extern crate failure;
#[macro_use]
extern crate slog;
extern crate slog_term;

use std::rc::Rc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::path::{Path, PathBuf};
use std::sync::Mutex;
use dyna_bindgen::analysis::{self, Bindings};
use dyna_bindgen::Driver;
use dyna_bindgen::generator::{Generator, GeneratorCtx};
use dyna_bindgen::utils;
use cargo::core::Workspace;
use failure::Error;
use slog::{Discard, Drain, Logger};

/// Disable logging during tests altogether.
const QUIET: bool = true;

fn dummy_crate() -> PathBuf {
    PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("tests/dummy")
}

fn test_logger() -> Logger {
    // When trying to figure out the args for rustc, cargo will try to run each job
    // in parallel. As a result, our `slog_term::TestStdoutWriter` is sometimes
    // written to from another thread. This is annoying because the test runner can
    // only capture (and suppress) output when it's generated from the main thread,
    // which means passing tests still spam the user.
    if QUIET {
        Logger::root(Discard, o!())
    } else {
        env_logger::try_init().ok();
        let decorator = slog_term::PlainDecorator::new(slog_term::TestStdoutWriter);
        let drain = slog_term::CompactFormat::new(decorator).build().fuse();
        let drain = Mutex::new(drain).fuse();

        Logger::root(drain, o!())
    }
}

#[test]
fn get_invocation_args() {
    let should_be = [
        "rustc",
        "--crate-name",
        "dummy",
        "tests/dummy/src/lib.rs",
        "--color",
        "never",
        "--crate-type",
        "lib",
        "--emit=dep-info,link",
        "-C",
        "debuginfo=2",
    ];

    let logger = test_logger();

    let dummy = dummy_crate().join("Cargo.toml");

    let cfg = utils::cargo_config().unwrap();
    let ws = Workspace::new(&dummy, &cfg).unwrap();
    let got = analysis::get_rustc_args(&ws, &cfg, &logger).unwrap();

    assert_eq!(
        got.len(),
        1,
        "We should only get 1 set of args, even if libc is recompiled"
    );
    let (ref args, ..) = got[0];

    let first_couple_args = &args[..should_be.len()];
    assert_eq!(first_couple_args, should_be);
}

#[test]
fn call_rustc_on_dummy() {
    let logger = test_logger();
    let dummy = dummy_crate().join("Cargo.toml");

    let mut called = 0;
    analysis::invoke_rustc_on_workspace(&dummy, &logger, |_, _| called += 1).unwrap();

    assert_eq!(called, 1);
}

#[test]
fn scan_dummy_for_ffi_stuff() {
    let logger = test_logger();
    let dummy = dummy_crate().join("Cargo.toml");

    analysis::invoke_rustc_on_workspace(&dummy, &logger, |state, _| {
        let hir = state.hir_crate.as_ref().unwrap();

        let bindings = analysis::scan(hir, &logger);

        assert_eq!(bindings.functions.len(), 2);
        assert_eq!(bindings.constants.len(), 1);
        assert_eq!(bindings.types.len(), 1);

        assert_eq!(bindings.warnings.len(), 2);
    }).unwrap();
}

#[test]
fn invoke_the_driver() {
    #[derive(Clone)]
    struct Gen(Rc<AtomicBool>);

    impl Generator for Gen {
        fn generate(
            &self,
            _bindings: &Bindings,
            _output_dir: &Path,
            _ctx: GeneratorCtx,
        ) -> Result<(), Error> {
            self.0.store(true, Ordering::Relaxed);
            Ok(())
        }

        fn name(&self) -> &str {
            "Gen"
        }
    }

    let g = Gen(Rc::new(AtomicBool::new(false)));

    let logger = test_logger();
    let dummy = dummy_crate().join("Cargo.toml");

    let mut driver = Driver::new_with_logger(dummy, &logger);
    driver.with_generator(g.clone());
    driver.run().unwrap();

    let was_called = g.0.load(Ordering::Relaxed);

    assert!(was_called);
}
