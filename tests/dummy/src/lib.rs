#![allow(private_no_mangle_fns, dead_code, unused_variables)]
extern crate libc;

use libc::c_int;

/// A very interesting number!
pub const NUMBER: u32 = 1234;

#[repr(C)]
#[derive(Debug, Clone)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

#[no_mangle]
pub extern "C" fn create_point(x: f64, y: f64) -> Point {
    Point { x, y }
}

#[no_mangle]
pub extern "C" fn create_points(n: c_int) -> *mut Vec<Point> {
    let points = Vec::new();
    Box::into_raw(Box::new(points))
}

#[no_mangle]
fn this_should_be_extern() {}

pub extern "C" fn missing_no_mangle() {}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub enum Direction {
    Up = 1,
    Down,
    Left,
    Right,
}

pub enum ComplexEnum {
    Dir(Direction),
    Empty,
    Struct { a: u32, b: Point },
}
